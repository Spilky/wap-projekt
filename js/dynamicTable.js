/**
 * Created by David Spilka (xspilk00) on 24.02.2016.
 */

function dynamicTable (id, activeColor) {
    var table = document.getElementById(id);

    var resizerX;
    var resizerY;
    var dragger;

    var draggerSource;
    var draggerTarget;

    var startXOffset;
    var startYOffset;

    activeColor = typeof activeColor !== 'undefined' ? activeColor : 'blue';
    table.className += ' dt-table';

    for (var i = 0; i < table.rows.length; i++) {
        for (var j = 0; j < table.rows[i].cells.length; j++) {
            var td = table.rows[i].cells[j];
            if (td.tagName.toLowerCase() === 'th' && i == 0) {
                td.style.cursor = 'move';

                td.addEventListener('mousedown', function (e) {
                    table.className += ' dt-disable-select';
                    if (!resizerX && !resizerY) {
                        dragger = this;
                    }
                });
            }

            var gripX = document.createElement('div');
            gripX.className = 'dt-gripX';
            gripX.innerHTML = "&nbsp;";
            gripX.addEventListener('mousedown', function (e) {
                table.className += ' dt-disable-select';
                resizerX = [];
                if (this.parentNode.parentNode.rowIndex == 0) {
                    var child = getChildIndexes(this.parentNode.cellIndex);
                    for (var k = 0; k < child.length; k++) {
                        resizerX.push(table.rows[table.rows.length-1].cells[child[k]]);
                    }
                } else {
                    resizerX.push(table.rows[table.rows.length-1].cells[this.parentNode.cellIndex]);
                }
                startXOffset = this.parentNode.offsetWidth - e.pageX;
            });
            td.appendChild(gripX);

            var gripY = document.createElement('div');

            gripY.className = 'dt-gripY';
            gripY.innerHTML = "&nbsp;";
            gripY.addEventListener('mousedown', function (e) {
                table.className += ' dt-disable-select';
                resizerY = table.rows[this.parentNode.parentNode.rowIndex].cells[0];
                startYOffset = this.parentNode.offsetHeight - e.pageY;
            });

            td.appendChild(gripY);
        }
    }

    document.addEventListener('mousemove', function (e) {
        if (resizerX) {
            for (var i = 0; i < resizerX.length; i++) {
                var style = window.getComputedStyle(resizerX[i], null);
                var newWidth = (startXOffset + e.pageX) / resizerX.length;
                var reduce = resizerX[i].offsetWidth - parseInt(style.getPropertyValue('width'));
                resizerX[i].style.width = newWidth - reduce + 'px';
            }
        }

        if (resizerY) {
            var style = window.getComputedStyle(resizerY, null);
            var newHeight = startYOffset + e.pageY;
            var reduce = resizerY.offsetHeight - parseInt(style.getPropertyValue('height'));
            resizerY.style.height = newHeight - reduce + 'px';
        }

        if (dragger) {
            for (var i = 0; i < table.rows[0].cells.length; i++) {
                var cellTh = table.rows[0].cells[i];
                if (e.pageX >= cellTh.offsetLeft && e.pageX <= cellTh.offsetLeft + cellTh.offsetWidth) {
                    draggerTarget = i;

                    cellTh.style.borderColor = activeColor;
                    if (cellTh.previousElementSibling) {
                        cellTh.previousElementSibling.style.borderRightColor = activeColor;
                    }

                    var child = getChildIndexes(i);
                    for (var j = 1; j < table.rows.length; j++) {
                        for (var k = 0; k < table.rows[j].cells.length; k++) {

                            var cell = table.rows[j].cells[k];
                            if (child.indexOf(k) >= 0) {
                                cell.style.borderColor = activeColor;
                                if (cell.previousElementSibling) {
                                    cell.previousElementSibling.style.borderRightColor = activeColor;
                                }
                            } else {
                                cell.style.borderColor = '';
                            }
                        }
                    }
                } else {
                    cellTh.style.borderColor = '';
                }

                if (cellTh == dragger) {
                    draggerSource = i;
                }
            }
        }
    });

    document.addEventListener('mouseup', function (e) {
        if (dragger && draggerSource != draggerTarget) {
            var sourceChild = getChildIndexes(draggerSource);
            var targetChild = getChildIndexes(draggerTarget);
            for (var i = table.rows.length - 1; i >= 0; i--) {
                if (draggerSource >  draggerTarget) {
                    if (i == 0) {
                        table.rows[i].insertBefore(table.rows[i].cells[draggerSource], table.rows[i].cells[draggerTarget]);
                    } else {
                        for (var j = 0; j < sourceChild.length; j++) {
                            table.rows[i].insertBefore(table.rows[i].cells[sourceChild[j]], table.rows[i].cells[targetChild[0]+j]);
                        }
                    }
                } else {
                    if (i == 0) {
                        table.rows[i].insertBefore(table.rows[i].cells[draggerSource], table.rows[i].cells[draggerTarget].nextSibling);
                    } else {
                        for (var j = 0; j < sourceChild.length; j++) {
                            table.rows[i].insertBefore(table.rows[i].cells[sourceChild[0]], table.rows[i].cells[targetChild[targetChild.length-1]].nextSibling);
                        }
                    }
                }
            }
        }

        for (var i = 0; i < table.rows.length; i++) {
            for (var j = 0; j < table.rows[i].cells.length; j++) {
                table.rows[i].cells[j].style.borderColor = '';
            }
        }
        table.className = table.className.replace(' dt-disable-select', '');

        draggerSource = undefined;
        draggerTarget = undefined;
        resizerX = undefined;
        resizerY = undefined;
        dragger = undefined;
    });

    function getChildIndexes(myX) {
        var fix = 0;
        var result = [];
        for (var i = 0; i < table.rows[0].cells.length; i++) {
            if (i == myX) {
                for (var j = 0; j < table.rows[0].cells[i].colSpan; j++) {
                    result.push(j + i + fix);
                }
                return result;
            }
            fix += table.rows[0].cells[i].colSpan - 1;
        }
    }
}

